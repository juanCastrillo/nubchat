import 'package:flutter/material.dart';
import 'package:nub_chat/src/ui/Inicio.dart';
import 'package:nub_chat/src/ui/Login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'src/blocs/MessageBloc.dart';

void main() {
  if(FirebaseAuth.instance.currentUser() != null) {
    final msgBloc = MessageBloc();
    runApp(Inicio(msgBloc));
  }
  else
    runApp(new Login());
}