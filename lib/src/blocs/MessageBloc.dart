import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:rxdart/subjects.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import '../models/Message.dart';
import '../resources/Conections.dart';


final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();


class MessageBloc extends Bloc<Message, Message>{


  Stream<List<Message>> get messages => _messagesSubject.stream;
  final _messagesSubject = BehaviorSubject<List<Message>>();
  final _messages = <Message>[];


  MessageBloc(){

    _firebaseMessaging.requestNotificationPermissions();
    _firebaseMessaging.subscribeToTopic("nub_chat");

    _firebaseMessaging.configure(
      onMessage: (map) {
        print(map);

        _messages.add(messageParser(map));
        _messagesSubject.add(_messages);

      },

      onLaunch: (map){},

      onResume: (map){},
      
    );

    _messagesSubject.add(_messages);

  }

  void sendMessage(Message msg){

    _messages.add(msg);
    Conections.sendMessage(msg);
    //_messagesSubject.add(_messages);

  }

  @override
  // TODO: implement initialState
  Message get initialState => null;

  @override
  Stream<Message> mapEventToState(Message state, Message event) {
    // TODO: implement mapEventToState
    return null;
  }


}