import 'User.dart';

class Message {

  User _user;
  User get user => _user;
  //set user(User user) => _user = user;

  String _messageBody;
  String get messageBody => _messageBody;
  //set messageBody(String messageBody) => _messageBody = messageBody;

  DateTime _sentMoment;
  DateTime get sentMoment => _sentMoment;
  //set sentMoment(DateTime sentMoment) => _sentMoment = sentMoment;

  Message(this._messageBody, this._user, this._sentMoment){}

}


//{notification: {title: , body: }, data: {}}
Message messageParser(Map<String, dynamic> message) {


  String messageBody = message['notification']['body'];
  User user = User();
  DateTime sentMoment;

  var dateTemp = message['notification']['date'];
  if(dateTemp != null)
     sentMoment = DateTime.parse(dateTemp);
  else
    sentMoment = DateTime.now();

  //print("aun no ha muerto");


  var msg = Message(
    messageBody,
    user,
    sentMoment,
  );

  //print("ahora si no ha muerto");

  return msg;
}