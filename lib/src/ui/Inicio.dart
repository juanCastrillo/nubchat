import 'package:flutter/material.dart';
import 'package:nub_chat/src/blocs/MessageBloc.dart';
import '../models/User.dart';
import '../models/Message.dart';

class Inicio extends StatelessWidget {

  final MessageBloc msgBloc;

  Inicio(this.msgBloc);


  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Chat',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new InicioPage(title: 'Chat Nub', bloc: msgBloc),
    );
  }
}

class InicioPage extends StatefulWidget {

  final MessageBloc bloc;
  final String title;

  InicioPage({Key key, this.title, this.bloc}) : super(key: key);


  @override
  _InicioPageState createState() => new _InicioPageState();
}

class _InicioPageState extends State<InicioPage> {

  var messageCntllr = new TextEditingController();

  void _addMessageUser(){

    var message = Message(
      messageCntllr.text,
      User(),
      DateTime.now()
    );

    widget.bloc.sendMessage(message);

    setState(() {
      messageCntllr.clear();
    });
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      /*appBar: new AppBar(
        title: new Text(widget.title),
      ),*/
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.amber,
                child: StreamBuilder<List<Message>>(
                  stream: widget.bloc.messages,
                  initialData: [],
                  builder: (context, snapshot) => ListView(
                    children: snapshot.data.map(message).toList(),
                  ),
                )
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    //padding: const EdgeInsets.all(16.0),
                    child: TextField(
                      controller: messageCntllr,
                      decoration: InputDecoration(
                          border: InputBorder.none, hintText: 'mensaje'),
                    ),
                  ),
                  //Expanded(child:

                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    color: Colors.amber,
                    onPressed: _addMessageUser, //(() => {}),
                    child: Icon(Icons.add),
                  ),
                  //)
                ],
              ),
            ),
          ],
        ),
      ),
      /*floatingActionButton: new FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: new Icon(Icons.add),
      ),*/
    );
  }

  Widget message(Message msg){
    return ListTile(
      onTap: (() => {}),
      title: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30.0)
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10.0),
              child: Text(
                '${msg.messageBody}',
                style: (TextStyle(fontSize: 12.0)),
              ),
            ),
          ),

          /*SentIndicator(

                          ),*/
        ],
      ),
    );
  }
}
