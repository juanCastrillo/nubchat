import 'package:flutter/material.dart';

enum Status {
  NOT_SENT,
  SENDING,
  SENT,
  RECEIVED,
  UNKNOWN
}

class SentIndicator extends StatelessWidget {

  Status state;

  SentIndicator(this.state){}

  @override
  Widget build(BuildContext context) {
    switch(state){

      case Status.NOT_SENT: {
        return Text("not send");
      }break;

      case Status.SENDING: {

      }break;

      case Status.SENT: {

      }break;

      case Status.RECEIVED: {

      }break;

      case Status.UNKNOWN:{

      }break;

    }
  }

}